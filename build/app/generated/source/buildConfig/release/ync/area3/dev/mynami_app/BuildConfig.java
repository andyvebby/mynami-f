/**
 * Automatically generated file. DO NOT MODIFY
 */
package ync.area3.dev.mynami_app;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "ync.area3.dev.mynami_app";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0.0";
}
