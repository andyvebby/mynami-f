import 'package:flutter/material.dart';
import 'package:mynami_app/splash_screen_view.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'MyNAMI',
      home: SplashScreenPage(),
    );
  }

}