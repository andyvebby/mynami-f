import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'package:mynami_app/home_view.dart';

class SplashScreenPage extends StatefulWidget {
  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

const TextStyle textStyle = TextStyle(
  color: Colors.blueGrey,
  fontFamily: 'Comfortaa',
);

class _SplashScreenPageState extends State<SplashScreenPage> with
    TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      duration: Duration(milliseconds: 3000),
      vsync: this,
    );

    animation = Tween(begin: 0.0, end: 1.0).animate(controller)
      ..addListener(() {
        setState(() {});
      });

    controller.forward();
//    startSplashScreen();
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  final background = Container(
    decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/mynami-bg.png'),
          fit: BoxFit.cover,
        )
    ),
  );

  final redOpacity = Container(
    color: Color(0xAAeee4e5),
  );

  Widget textfield(String label, Function onTap) {
    return new FadeTransition(
      opacity: animation,
      child: new SlideTransition(
        position: Tween<Offset>(begin: Offset(0.0, -0.6), end: Offset.zero)
            .animate(controller),
        child: Material(
          color: Color(0xBBc50a0a),
          borderRadius: BorderRadius.circular(30.0),
          child: InkWell(
            onTap: onTap,
            borderRadius: BorderRadius.circular(30.0),
            splashColor: Colors.orange,
            highlightColor: Colors.white30,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 13.0, horizontal: 13.0),
              child: Center(
                child: TextField(
                  autofocus: false,
                  keyboardType: TextInputType.number,
                  maxLength: 13,
                  decoration: InputDecoration(
                      icon: Icon(
                        Icons.perm_phone_msg,
                        color: Colors.white,
                        size: 30.0,),
                      hintText: label,
                      labelText: 'UserID: +62',
                      labelStyle: textStyle.copyWith(fontSize: 20.0, color: Colors.white),
                      counterText: '',
                      border: InputBorder.none,
                      hintStyle: textStyle.copyWith(fontSize: 20.0, color: Colors.white)),
                  style: textStyle.copyWith(fontSize: 30.0, color: Colors.white),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget button(String label, Function onTap) {
    return new FadeTransition(
      opacity: animation,
      child: new SlideTransition(
        position: Tween<Offset>(begin: Offset(0.0, -0.6), end: Offset.zero)
            .animate(controller),
        child: Material(
          color: Color(0xBBc50a0a),
          borderRadius: BorderRadius.circular(30.0),
          child: InkWell(
            onTap: onTap,
            borderRadius: BorderRadius.circular(30.0),
            splashColor: Colors.orange,
            highlightColor: Colors.white30,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 13.0),
              child: Center(
                child: Text(
                  label,
                  style: textStyle.copyWith(fontSize: 20.0, color: Colors.white),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

//  startSplashScreen() async {
//    var duration = const Duration(seconds: 5);
//    return Timer(duration, () {
//      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
//        return HomePage();
//      }));
//    });
//  }
//
  @override
  Widget build(BuildContext context) {
//    Size size = MediaQuery.of(context).size;
//    // TODO: implement build
//    return Scaffold(
//      backgroundColor: Color(0xff2b5e),
//      body: Center(
//        child: Image.asset("assets/images/mynami-bg.png",
//          width: size.width,
//          height: size.height,
//          fit: BoxFit.fill),
//      ),
//    );
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown
    ]);

    final logo = new ScaleTransition(
      scale: animation,
      child: Image.asset('assets/images/mynami-logo.png',
        width: 180.0,
        height: 230.0,),
    );

    final description = new FadeTransition(
      opacity: animation,
      child: new SlideTransition(
        position: Tween<Offset>(begin: Offset(0.0, -0.8), end: Offset.zero)
            .animate(controller),
        child: Text('Your focus determines your reality. - Qui-Gonn Jinn',
          textAlign: TextAlign.center,
          style: textStyle.copyWith(fontSize: 24.0),),
      ),
    );

    final separator = new FadeTransition(
      opacity: animation,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(width: 20.0, height: 2.0, color: Colors.white),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8.0),
            child: Text('OR',
              style: textStyle,),
          ),
          Container(width: 20.0, height: 2.0, color: Colors.white,)
        ],
      ),
    );

    final signWith = new FadeTransition(
      opacity: animation,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Sign up with',
            style: textStyle,),
          GestureDetector(
            onTap: () {
              print('namicare');
            },
            child: Text('Namicare',
              style: textStyle.copyWith(
                  color: Color(0xBB930000),
                  fontWeight: FontWeight.bold,
                  decoration: TextDecoration.underline
              ),
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          background,
          redOpacity,
          new SafeArea(
            child: new Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  logo,
                  SizedBox(height: 40.0,),
                  description,
                  SizedBox(height: 60.0,),
                  textfield('8xxx', () {
                    print('account');
                  }),
                  SizedBox(height: 8.0,),
                  button('Sign In', () {
                    print('sign in');
                  }),
                  SizedBox(height: 30.0,),
                  separator,
                  SizedBox(height: 30.0,),
                  signWith
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}